//
//  DigiListTests.swift
//  DigiListTests
//
//  Created by Axel Roest on 25/03/2019.
//  Copyright © 2019 Phluxus. All rights reserved.
//

import XCTest
@testable import DigiList

class DigiListTests: XCTestCase {

    func testBadIdentity() {
        let testBundle = Bundle(for: type(of: self))
        let url = testBundle.url(forResource: "badidentity", withExtension: "json")
        XCTAssertNotNil(url)
        let data = try! Data.init(contentsOf: url!)
        let decoder = JSONDecoder()
        XCTAssertThrowsError(try decoder.decode(Identity.self, from: data))
    }
    
    func testIdentityJsonIntegrity() {
        let testBundle = Bundle(for: type(of: self))
        let url = testBundle.url(forResource: "identity", withExtension: "json")
        XCTAssertNotNil(url)
        let data = try! Data.init(contentsOf: url!)
        let decoder = JSONDecoder()
        
        let identity = try! decoder.decode(Identity.self, from: data)
        
        XCTAssertEqual(identity.id, "5c35f208ee32c")
        XCTAssertEqual(identity.text, "30. elteu")
        XCTAssertEqual(identity.confidence, 0.69, accuracy: 0.001)
        XCTAssertNotNil(identity.image)
    }
    
}
