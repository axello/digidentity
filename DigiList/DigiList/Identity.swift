//
//  Identity.swift
//  DigiList
//
//  Created by Axel Roest on 25/03/2019.
//  Copyright © 2019 Phluxus. All rights reserved.
//

import UIKit
//import Realm

class Identity: NSObject, Decodable {
    @objc dynamic var text = ""
    @objc dynamic var confidence: Double = 0
    @objc dynamic var id = ""
    @objc dynamic var imageData = ""
    
    var imageStore: UIImage?
    
    private enum CodingKeys : String, CodingKey {
        case text
        case confidence
        case imageData = "img"
        case id = "_id"
    }

    var image: UIImage {
        if let image = imageStore {
            return image
        }
        guard imageData.count > 0, let data = Data(base64Encoded: imageData), let image = UIImage.init(data: data) else {
            return UIImage(named: "axel")!
        }
        imageStore = image
        return image
    }
    
    class func mocked() -> Identity {
        let identity = Identity()
        identity.id = "jsldfsdflk"
        identity.text = "test naam"
        identity.confidence = Double.random(in: 0.0..<1.0)
        return identity
    }
}
