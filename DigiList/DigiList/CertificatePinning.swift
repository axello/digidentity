
import Foundation
import Alamofire

class CertificatePinning {
    private let url: URL
    
    init(url: URL) {
        self.url = url
    }
    
    /// Set up trust manager for the host in the url string
    ///
    /// - Parameter urlString: a url string
    /// - Throws: when the urlString does not contain a proper url
    func createTrustManagerForSSLPinning() throws -> ServerTrustPolicyManager {
        guard let hostName = url.host else {
            throw ServiceError.invalidUrl
        }
        return try createTrustManagerForSSLPinning(hostName: hostName)
    }
    
    /// Set up trust manager for the host name
    /// As the trust manager works with hostnames, this is the designated method to use
    /// - Parameter hostName: a host name, so *without* any https part
    /// - Throws: when it cannot find any certificates
    func createTrustManagerForSSLPinning(hostName: String) throws -> ServerTrustPolicyManager {
        let certificates = ServerTrustPolicy.certificates(in: Bundle.main)
        guard !certificates.isEmpty else {
            throw ServiceError.internalError
        }
        
        let serverTrustPolicy = ServerTrustPolicy.pinCertificates(
            certificates: certificates,
            validateCertificateChain: true,
            validateHost: true
        )
        let serverTrustPolicies = [hostName: serverTrustPolicy]
        
        return ServerTrustPolicyManager(policies: serverTrustPolicies)
    }
}
