//
//  Services.swift
//  DigiList
//
//  Created by Axel Roest on 25/03/2019.
//  Copyright © 2019 Phluxus. All rights reserved.
//

import Foundation
import Alamofire

class Services {
    static let shared = Services()
    
    let baseURL: URL = URL(string: "https://marlove.net/e/mock/v1/")!
    let apiKey: String = "f3bb807182fcc003c81406d8c70c411d"
    private let manager: SessionManager
    
    init() {
        let urlSessionConfiguration = URLSessionConfiguration.default
        urlSessionConfiguration.httpAdditionalHeaders = [
            "Authorization": apiKey
        ]

        let diskPath = try! FileManager.default.url(for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: true).absoluteString
        urlSessionConfiguration.urlCache = URLCache(memoryCapacity: 2 * 1024 * 1024, diskCapacity: 128 * 1024 * 1024, diskPath: diskPath)
        var serverTrustPolicyManager: ServerTrustPolicyManager?
        let certificatePinning = CertificatePinning(url: baseURL)
        do {
            serverTrustPolicyManager = try certificatePinning.createTrustManagerForSSLPinning()
        } catch ServiceError.invalidUrl {
            assertionFailure("invalid host name")
        } catch {
            assertionFailure("invalid certificate")
        }

        manager = Alamofire.SessionManager(configuration: urlSessionConfiguration, serverTrustPolicyManager: serverTrustPolicyManager)
        
    }

    // The assignment talks about 'most recent' and 'oldest' items,
    // but as there is neither linear id, nor a timestamp field,
    // it is left to guess work and assumptions what they mean by that.
    // the backend returns ids from 30 downwards
    func fetchIdentities(from: String? = nil, upTo: String? = nil, cache: Bool = true, completion: @escaping(Result<[Identity], ServiceError>) -> Void) {
        let url = baseURL.appendingPathComponent("items")
        var parameters: Parameters?
        if let from = from {
            parameters = ["since_id": from]
        }
        if let upTo = upTo {
            parameters = parameters == nil ? Parameters() : parameters
            parameters?["max_id"] = upTo
        }
//        let response = manager.session.configuration.urlCache?.cachedResponse(for: URLRequest(url: url))
        
        manager.request(url, method: .get, parameters: parameters)
            .validate()
            .responseJSON { response in
            if let error = response.result.error {
                completion(Result.failure(ServiceError.fetchFailed(error)))
            } else {
                if let json = response.data {
                    if let urlResponse = response.response {
                        let cachedResponse = CachedURLResponse(response: urlResponse, data: json)
                        URLCache.shared.storeCachedResponse(cachedResponse, for: response.request!)
                    }
                    do {
                        let decoder = JSONDecoder()
                        let returnedDecodable: [Identity] = try decoder.decode([Identity].self, from: json)
                        completion(Result.success(returnedDecodable))
                    } catch {
                        completion(Result.failure(ServiceError.jsonError))
                    }
                } else {
                    completion(Result.failure(ServiceError.jsonError))
                }
            }
        }
    }

    func delete(identityId: String, completion: @escaping(Result<Void, ServiceError>) -> Void) {
        guard identityId.count > 0 else {
            // all identities are wiped if we forget the id parameter; what a stupid backend design, alas.
            completion(Result.failure(ServiceError.internalError))
            return
        }
        let url = baseURL.appendingPathComponent("item").appendingPathComponent(identityId)
        manager.request(url, method: .delete)
            .validate()
            .response { response in
                if let error = response.error {
                    completion(Result.failure(ServiceError.fetchFailed(error)))
                } else {
                    if response.response?.statusCode == 200 {
                        completion(Result.success(Void()))
                    }
                }
        }

    }
}
