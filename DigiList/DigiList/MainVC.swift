//
//  ViewController.swift
//  DigiList
//
//  Created by Axel Roest on 25/03/2019.
//  Copyright © 2019 Phluxus. All rights reserved.
//

import UIKit

class MainVC: UITableViewController {

    let dataSource = MainDataSource()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = dataSource

        tableView.delegate = self
        
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        tableView.refreshControl = refreshControl

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        loadIdentities()
    }
    
    @objc func refresh(_ refreshControl: UIRefreshControl) {
        loadIdentities()
    }

    func loadIdentities() {
        Services.shared.fetchIdentities { [weak self] result in
            self?.refreshControl?.endRefreshing()
            switch result {
            case .failure:
                return
            case .success(let identities):
                self?.dataSource.identities = identities
                self?.tableView.reloadData()
            }
        }
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85.0
    }

    /// We use this to determine if the user scrolled. Then when the list is at the end, we fetch new entries
    override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {            let actualPosition = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height - self.tableView.frame.size.height
        if actualPosition >= contentHeight {
            dataSource.fetchMore()
        }
    }

}
