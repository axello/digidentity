//
//  ServiceError.swift
//  DigiList
//
//  Created by Axel Roest on 25/03/2019.
//  Copyright © 2019 Phluxus. All rights reserved.
//

import Foundation

enum ServiceError: Error {
    case fetchFailed(Error)
    case noInternet(Error)
    case jsonError
    case invalidUrl
    case internalError
}
