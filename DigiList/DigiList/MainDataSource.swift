//
//  MainDataSource.swift
//  DigiList
//
//  Created by Axel Roest on 25/03/2019.
//  Copyright © 2019 Phluxus. All rights reserved.
//

import UIKit
//import RealmSwift

class MainDataSource: NSObject {
    var identities = [Identity]()
    var tableView: UITableView?     // for refresh purposes
    public var updated: Int = 0
    
    func mockInit() {
        for _ in (0...10) {
            identities.append(Identity.mocked())
        }
    }
    
    func fetchMore() {
        if let id = identities.last?.id {
            appendIdentities(from: id)
        }
    }
    
    func appendIdentities(from id: String) {
        Services.shared.fetchIdentities(upTo: id) { [weak self] result in
            switch result {
            case .failure:
                return
            case .success(let identities):
                self?.mergeIdenties(newIdentities: identities)
                self?.updated = (self?.identities.count)!
                self?.tableView?.reloadData()
            }
        }
    }
    
    func mergeIdenties(newIdentities: [Identity]) {
        // not a merge, per sé, as  it is unspecified if identities can occur twice in the list, so we append
        for identity in newIdentities {
            identities.append(identity)
        }
    }

}

extension MainDataSource: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableView = tableView
        return identities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identityCell = tableView.dequeueReusableCell(withIdentifier: IdentityCell.reuseIdentifier, for: indexPath) as! IdentityCell
        identityCell.configure(with: identities[indexPath.row])

        return identityCell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCell.EditingStyle.delete) {
            Services.shared.delete(identityId: identities[indexPath.row].id) { [weak self] result in
                switch result {
                case .failure:
                    return
                case .success:
                    self?.identities.remove(at: indexPath.row)
                    self?.tableView?.deleteRows(at: [indexPath], with: .fade)
                }

            }
        }
    }

}
