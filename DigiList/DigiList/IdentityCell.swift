//
//  IdentityCell.swift
//  DigiList
//
//  Created by Axel Roest on 25/03/2019.
//  Copyright © 2019 Phluxus. All rights reserved.
//

import UIKit

class IdentityCell: UITableViewCell {
    
    static let reuseIdentifier = "IdentityCell"
    @IBOutlet weak var mainTextLabel: UILabel!
    @IBOutlet weak var confidenceLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configure(with identity: Identity) {
        mainTextLabel.text = identity.text
        confidenceLabel.text = "confidence: \(identity.confidence)"
        idLabel.text = identity.id
        iconImageView.image = identity.image
    }
}
